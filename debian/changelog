starfighter (2.4-1) unstable; urgency=medium

  * New upstream version: 2.4.
  * debian/control: Bumped Standards version to 4.6.1.
  * debian/source: created lintian-overrides file, to account for lintian
    false positive for very-long-line-length-in-source-file.
  * Removed patch 010_fix-autoconf-flags.patch; changes were incorporated
    by upstream.

 -- Francisco M Neto <fmneto@fmneto.com>  Thu, 10 Jun 2021 01:46:38 +0000

starfighter (2.3.3-1) unstable; urgency=medium

  * New maintainer; thanks to Guus Sliepen for all your work! (Closes: #963930)
  * New upstream version. (Closes: #858796)
  * debian/clean: created file.
  * debian/control:
    - Added fonts-takao-gothic to Depends: line.
    - Added Vcs-fields.
    - Bumped Standards version to 4.5.1
    - Changed debhelper to debhelper compat and bumped version to 13.
    - Removed debian/compat file.
    - Updated Homepage field.
  * debian/copyright: updated copyright fields.
  * debian/not-installed: suppressing dh_missing messages for unnecessary files.
  * debian/patches:
    - Codebase has been rewritten, removing all patches.
    - Added 010_fix-autoconf-flags.patch to ensure 'bindnow' when linking
      binaries.
  * debian/rules:
    - Added autoreconf with override and removed --parallel.
    - Added DEB_BUILD_MAINT_OPTIONS with hardening=+all.
  * debian/starfighter.links: created link to font file from fonts-takao-gothic.
  * debian/upstream:
    - Created metadata file.
    - Updated signature-key.asc file.
  * debian/watch: updated to track correct URI.
  * debian/*.install: reconstructed both install files to account for
    rewritten source code.

 -- Francisco M Neto <fmneto@fmneto.com>  Thu, 27 Aug 2020 01:05:10 +0000

starfighter (1.7-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version and debian/compat.
  * Add Rules-Requires-Root: no.
  * Set package priority to optional.
  * Fix some spelling errors.
  * Fix warnings given by GCC.

 -- Guus Sliepen <guus@debian.org>  Sun, 21 Jan 2018 23:20:01 +0100

starfighter (1.6-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version.

 -- Guus Sliepen <guus@debian.org>  Sun, 21 Jan 2018 22:45:07 +0100

starfighter (1.5.1.1-1) unstable; urgency=medium

  * New upstream release.
    - Provides a .desktop file. Closes: #738033

 -- Guus Sliepen <guus@debian.org>  Sun, 24 Jan 2016 23:07:10 +0100

starfighter (1.4-1) unstable; urgency=medium

  * New upstream release.
  * Updated debian/watch file.
  * Drop debian/patches/install-music, which has been applied upstream.

 -- Guus Sliepen <guus@debian.org>  Sat, 04 Jul 2015 23:22:13 +0200

starfighter (1.3.1-1) unstable; urgency=medium

  * New upstream release from http://starfighter.nongnu.org/.
  * Bump Standards-Version and debian/compat.
  * Enable parallel builds.
  * Update debian/copyright.
  * Switch to SDL2.
  * Ensure the new DFSG-compliant music is installed.
  * Added a debian/watch file.

 -- Guus Sliepen <guus@debian.org>  Mon, 20 Apr 2015 18:21:49 +0200

starfighter (1.2-2) unstable; urgency=low

  * Include a full copy of the CC-BY and CC-BY-SA 3.0 license texts.

 -- Guus Sliepen <guus@debian.org>  Sun, 04 Mar 2012 15:38:28 +0100

starfighter (1.2-1) unstable; urgency=low

  * New upstream release.
    - DFSG-compatible graphics and sounds.
    - Music has been removed.

 -- Guus Sliepen <guus@debian.org>  Sun, 26 Feb 2012 23:16:58 +0100

starfighter (1.1-7) unstable; urgency=low

  [ Eddy Petrișor ]
  * fix Homepage semifield

  [ Gonéri Le Bouder ]
  * update 10_nobusyloop.diff to fix the build with gcc 4.2
    thanks Brian m. Carlson and Jens Seidel(Closes: #385714)
  * make the package binNMU by replace starfighter-data (= ${Source-Version})
    by starfighter-data (= ${source:Version}), thanks Lior Kaplan
    (Closes: #435958)
  * make clean removes the starfighter binary to be able to do two builds in a
    row (Closes: #424236)
  * do not ignore make clean returned value anymore
  * add myself in Uploaders:
  * remove quilt .pc directory
  * remove some useless debhelper call

  [ Jon Dowland ]
  * update menu section to "Games/Action" for menu policy transition.
    Thanks Linas Žvirblis.

  [ Cyril Brulebois ]
  * Added XS-Vcs-Svn and XS-Vcs-Browser fields in the control file.

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sat, 22 Sep 2007 10:27:49 +0000

starfighter (1.1-6) unstable; urgency=low

  * debian/patches/01_makefile.diff:
    + Properly honour DEB_BUILD_OPTIONS.

  * debian/patches/10_sigfpe.diff:
    + New patch. Fix a SIGFPE occurring with slow projectiles (Closes: #254117,
      Closes: #372924).

  * debian/patches/10_overflows.diff:
    + New patch. Fix overflows in weapon count operations (Closes: #286934).

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Thu, 22 Jun 2006 14:55:16 +0200

starfighter (1.1-5) unstable; urgency=low

  * debian/control:
    + I said I added a build-depend on python, but I lied (Closes: #373852).

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Sun, 18 Jun 2006 18:35:24 +0200

starfighter (1.1-4) unstable; urgency=low

  * Adopted package and moved it to the Debian Games Team repository.
  * Set Bartosz Fenski, Alexander Schmehl and myself as uploaders.
  * Moved patch system to quilt.

  * debian/control:
    + Set policy to 3.7.2.
    + Build-depend on quilt instead of dpatch.
    + Build-depend on python because we unpack the .pak file in order to
      fix the errors in it.

  * debian/starfighter.xpm:
    + Created an icon and added it to the menu (Closes: #319194).

  * debian/starfighter.6:
    + Renamed starfighter.1 into starfighter.6.

  * debian/patches/01_makefile.diff:
    + Install the patched .pak file instead of the original one.

  * debian/patches/10_nobusyloop.diff:
    + New patch from Guus Sliepen. Use SDL_Delay() instead of busy loops to
      reduce CPU usage (Closes: #366029).

  * debian/patches/30_typos.diff:
    + Fixed typos in the documentation and in the in-game messages
      (Closes: #266588, #267103, #372925).

  * debian/pack.py debian/unpack.py:
    + New patch. Include scripts courtesy of Josh Triplett to pack/unpack
      data files and ship them with the -data package (Closes: #358706).

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Thu, 15 Jun 2006 19:22:30 +0200

starfighter (1.1-3) unstable; urgency=low

  * rewritten description of documentation (Closes: #296671)
    Thanks to Pierre Thierry for spotting this.
  * debian/starfighter.menu:
    - added longtitle
  * s/fenio@o2.pl/fenio@debian.org/

 -- Bartosz Fenski <fenio@debian.org>  Sat, 26 Feb 2005 14:26:12 +0100

starfighter (1.1-2) unstable; urgency=low

  * debian/control:
    - iteration of features has been adjusted to disallow line wraping.
      Thanks to Enrico Zini for pointing it out to me. (Closes: #253820)
    - removed unused ${misc:Depends} variable
    - we're now using ${Source-Version} for dependency
  * debian/rules:
    - binary targets has been splitted to indep and arch sections.

 -- Bartosz Fenski <fenio@o2.pl>  Fri, 11 Jun 2004 13:12:17 +0200

starfighter (1.1-1) unstable; urgency=low

  * Initial Release. (Closes: #243010)

 -- Bartosz Fenski <fenio@o2.pl>  Sun, 11 Apr 2004 13:18:14 +0200
